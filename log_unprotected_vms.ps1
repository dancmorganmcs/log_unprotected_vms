#written by dan morgan 12/2/2021 macquarie cloud services
#script needs automationaccount to have azaccounts, azoperationalinsights and azrecoveryservices modules

#user parameters
param(
    [Parameter(Mandatory = $true)]
    [string] $LogAnalyticsWorkspaceName
)

#create array to hold vm names
$unprotectedvmnames = [System.Collections.ArrayList]@()
$protectedvmnames = [System.Collections.ArrayList]@()
$allvmnames = [System.Collections.ArrayList]@()

#login
#<#
Disable-AzContextAutosave –Scope Process
$connectionName = "AzureRunAsConnection"
Try
{
    # Get the connection "AzureRunAsConnection"
    $servicePrincipalConnection = Get-AutomationConnection -name AzureRunAsConnection 

    "Logging in to Azure..."
    Add-AzAccount `
        -ServicePrincipal `
        -SubscriptionId $servicePrincipalConnection.SubscriptionId `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
        -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 
}
Catch {
    If (!$servicePrincipalConnection)
    {
        $ErrorMessage = "Connection $connectionName not found."
        throw $ErrorMessage
    } Else {
        Write-Error -Message $_.Exception
        Throw $_.Exception
    }
}
##>

#Manual login for testing
<#
#contoso
$subscriptionid="97f3768f-9c10-476a-973b-fcadf178d70a"
$outputstring = "Connecting to subscription ID $subscriptionid..."
Write-Output $outputstring
try {
	Connect-AzAccount -SubscriptionID $subscriptionid
}
catch {
	$outputstring = "Unable to connect to subscription. Exiting..."
	Write-Output $outputstring
	exit
}
#>

#get the workspace ID and keys to actually write to the workspace
#cycle through all resource groups to find the one provided in the parameters
foreach ($rg in Get-AzResourceGroup) {
    #cycle through all the workspaces in the current resource group to find the one that matches parameter name
    foreach ($currentlaw in Get-AzOperationalInsightsWorkspace -ResourceGroupName $rg.ResourceGroupName) {
        $rgname = $rg.ResourceGroupName
        $currentlawname = $currentlaw.name
        if ($currentlawname -like $LogAnalyticsWorkspaceName) {
            #if we found a match, gather the law ID
            $lawobj = Get-AzOperationalInsightsWorkspace -ResourceGroupName $rgname -name $currentlawname
            $LogAnalyticsWorkspaceID = $lawobj.CustomerId
            #if we found a match, gather the law primary key
            $lawkeys = Get-AzOperationalInsightsWorkspaceSharedKey -ResourceGroupName $rgname -name $currentlawname
            $LogAnalyticsWorkspaceKey = $lawkeys.PrimarySharedKey
        }
    }
}

#get all VMs
$outputstring = "Getting all VMs in subscription..."
Write-Output $outputstring
foreach ($rg in Get-AzResourceGroup) {
	foreach ($vm in Get-AzVM -ResourceGroupName $rg.ResourceGroupName) {
		$vmname = $vm.Name -replace "`n","" -replace "`r",""
		$allvmnames.Add($vmname) | Out-Null
	}
}
$outputstring = "Got all VMs."
Write-Output $outputstring

#get all protected VMs
#loop through every vault in subscription
$vmcounter = 0
$outputstring = "Examining vaults for protected VMs..."
Write-Output $outputstring
foreach ($vault in Get-AzRecoveryServicesVault) {
	#loop through every fabric in each vault
	$vaultname = $vault.Name
	#Write-Output "Examining vault $vaultname for VMs"
	Set-AzRecoveryServicesAsrVaultContext -Vault $vault | Out-Null
	foreach ($fabric in Get-AzRecoveryServicesAsrFabric) {
		#loop through each container in each fabric
		foreach ($container in Get-AzRecoveryServicesAsrProtectionContainer -Fabric $fabric) { 
			#loop through each VM in each container
			foreach ($protecteditem in Get-AzRecoveryServicesAsrReplicationProtectedItem -ProtectionContainer $container) {
				$vmname = $protecteditem.FriendlyName -replace "`n","" -replace "`r",""
				#Write-Output "Found VM! Adding $vmname to protected list..."
				$vmcounter++
				try {
					$protectedvmnames.Add($vmname) | Out-Null
				}
				catch {
					Write-Output "" | Out-Null
				}
			}
		}
	}
	$outputstring = "Done with $vaultname, got $vmcounter protected VMs."
	#Write-Output $outputstring
	$vmcounter = 0
}

#Find unprotected VMs
foreach ($vm in $allvmnames) {
	if ($protectedvmnames -contains $vm) {
		#if VM is a protected VM then do nothing
		$outputstring = "$vm is in protectedvmnames"
		#Write-Output $outputstring
	} else {
		#if VM is not a protected VM then add to the list of unprotected vms
		$outputstring = "$vm is not in protectedvmnames"
		#Write-Output $outputstring
		$unprotectedvmnames.Add($vm) | Out-Null
	}
}

#display list of VMs that are not replicating
#foreach ($vm in $unprotectedvmnames) {
#	$outputstring = "$vm is not protected"
#	Write-Output $outputstring
#}

$outputstring = "$unprotectedvmnames are not protected"
Write-Output $outputstring

# https://docs.microsoft.com/en-au/azure/azure-monitor/platform/data-collector-api#powershell-sample?WT.mc_id=EM-MVP-5002871
# Replace with your Workspace ID
#$LogAnalyticsWorkspaceID = "f9770c51-b7ce-4f26-a8b1-ff2320423034"  

# Replace with your Primary Key
#$LogAnalyticsWorkspaceKey = "5pv6pmg2Und93fSgRTd8rET0s+loNwrW5eiyClIbKntj740B8X4GAAb50a0SrqxDmU6B2VIi7W6y6ncqMczeJQ=="

# Specify the name of the record type that you'll be creating
$LogType = "UnprotectedVMs"

# You can use an optional field to specify the timestamp from the data. If the time field is not specified, Azure Monitor assumes the time is the message ingestion time
$TimeStampField = ""

# Create record
$json = @"
[{	"UnprotectedVMs": "$unprotectedvmnames"
}]
"@

# Create the function to create the authorization signature
Function Build-Signature ($LogAnalyticsWorkspaceID, $LogAnalyticsWorkspaceKey, $date, $contentLength, $method, $contentType, $resource)
{
    $xHeaders = "x-ms-date:" + $date
    $stringToHash = $method + "`n" + $contentLength + "`n" + $contentType + "`n" + $xHeaders + "`n" + $resource

    $bytesToHash = [Text.Encoding]::UTF8.GetBytes($stringToHash)
    $keyBytes = [Convert]::FromBase64String($LogAnalyticsWorkspaceKey)

    $sha256 = New-Object System.Security.Cryptography.HMACSHA256
    $sha256.Key = $keyBytes
    $calculatedHash = $sha256.ComputeHash($bytesToHash)
    $encodedHash = [Convert]::ToBase64String($calculatedHash)
    $authorization = 'SharedKey {0}:{1}' -f $LogAnalyticsWorkspaceID,$encodedHash
    return $authorization
}

# Create the function to create and post the request
Function Post-LogAnalyticsData($LogAnalyticsWorkspaceID, $LogAnalyticsWorkspaceKey, $body, $logType)
{
    $method = "POST"
    $contentType = "application/json"
    $resource = "/api/logs"
    $rfc1123date = [DateTime]::UtcNow.ToString("r")
    $contentLength = $body.Length
    $signature = Build-Signature `
        -LogAnalyticsWorkspaceID $LogAnalyticsWorkspaceID `
        -LogAnalyticsWorkspaceKey $LogAnalyticsWorkspaceKey `
        -date $rfc1123date `
        -contentLength $contentLength `
        -method $method `
        -contentType $contentType `
        -resource $resource
    $uri = "https://" + $LogAnalyticsWorkspaceID + ".ods.opinsights.azure.com" + $resource + "?api-version=2016-04-01"
    $headers = @{
        "Authorization" = $signature;
        "Log-Type" = $logType;
        "x-ms-date" = $rfc1123date;
        "time-generated-field" = $TimeStampField;
    }
    $response = Invoke-WebRequest -Uri $uri -Method $method -ContentType $contentType -Headers $headers -Body $body -UseBasicParsing
    return $response.StatusCode

}

$outputstring = "Sending unprotected VM list log to analytics workspace $LogAnalyticsWorkspaceID"
Write-Output $outputstring
# Submit the data to the API endpoint
$statuscode = Post-LogAnalyticsData -LogAnalyticsWorkspaceID $LogAnalyticsWorkspaceID -LogAnalyticsWorkspaceKey $LogAnalyticsWorkspaceKey -body ([System.Text.Encoding]::UTF8.GetBytes($json)) -logType $logType
$outputstring = "Received HTTP Response Code $statuscode from Azure"
Write-Output $outputstring
$outputstring = "Script Finished."
Write-Output $outputstring